
//Settings
var AFFICHER_CALCUL = false;
var POSITION_MODE='manuel';
var CALCUL_MODE='quotidien';


//Constantes
var E0      = 23 + 27/60;
var V0      = 0 + 30/60;





function radian(val){
  return val * math.PI / 180.0;
}

function degres(val){
  return 180.0 * val / math.PI;
}

function calculPmLevCou(S, latitude, longitude){
  //R3
  var M         = 357 + 0.9856 * S;
  var G         = 1.94 * math.sin(radian(M)) + 0.02 * math.sin(radian(2.0 * M));
  var L         = 280 + 0.9856 * S + G;
  var R         = -2.465 * math.sin(radian(2.0 * L)) + 0.053 * math.sin(radian(4.0 * L));
  var C         = 4.0 * ( G + R );
  //R4
  var N, DS, K, A, EJ;

  if(S>=0 && S<79){
    N = 5;
  }
  else if(S>=79 && S<172){
    N = 1;
  }
  else if(S>=172 && S<266){
    N = 2;
  }
  else if(S>=266 && S<355){
    N = 3;
  }
  else if(S>=355 && S<=365){
    N = 4;
  }
  else{
    N=0;
  }

  switch (N) {
    case 5:
      DS = S - 79;
      K  = 90.0/89.0;
      break;
    case 4:
      DS = S - 444;
      K  = 90.0/89.0;
      break;
    case 3:
      DS = S - 355 - 89;
      K  = 90.0/89.0;
      break;
    case 2:
      DS = S - 172 + 93;
      K  = 90.0/94.0;
      break;
    case 1:
      DS = S - 79;
      K  = 90.0/93.0;
      break;
    default:
      DS = 0;
      K  = 0;
      break;

  }

  A = DS * K;
  EJ = degres(math.asin( math.sin(radian(A)) * math.sin(radian(E0))));

  //R5
  var B;
  var DT;
  var K;

  K  = math.sin(radian(V0)) / math.cos(radian(latitude));
  DT = degres(math.asin(math.tan(radian(latitude)) * math.tan(radian(EJ)) + K));
  B  = 6 + DT/15.0;
  //R6
  var PM;
  var COU;
  var LEV;
  PM    = 12 + C/60.0 - longitude/15.0; while(PM<0) PM+=24; while(PM>24) PM-=24;
  COU   = PM + B;while(COU<0) COU+=24;while(COU>24) COU-=24;
  LEV   = PM - B;while(LEV<0) LEV+=24;while(LEV>24) LEV-=24;

  var resultat ={
    'PM' : PM,
    'LEV': LEV,
    'COU': COU
  }

  if (AFFICHER_CALCUL){

    console.log('R1');
    console.log('\t Latitude : '+latitude);
    console.log('\t Longitude : '+longitude);

    console.log('R2');
    console.log('\tS : '+S);

    console.log('R3');
    console.log('\tM : '+M);
    console.log('\tG : '+G);
    console.log('\tL : '+L);
    console.log('\tR : '+R);
    console.log('\tC : '+C);

    console.log('R4');
    console.log('\tE0 : '+E0);
    console.log('\tN : '+N);
    console.log('\tDS : '+DS);
    console.log('\tK : '+K);
    console.log('\tA : '+A);
    console.log('\tEJ : '+EJ);

    console.log('R5');
    console.log('\tV0 : '+V0);
    console.log('\tK : '+K);
    console.log('\tDT : '+DT);
    console.log('\tB : '+B);

    console.log('R6');
    console.log('\tPM : '+PM);
    console.log('\tLEV : '+LEV);
    console.log('\tCOU : '+COU);
  }

  return resultat;

}



function getLatLonFromDisplay(){
  var longitude, latitude;
  //R1
  switch(POSITION_MODE){
    case 'wilaya':
      var wilayaCode = $('#wilaya-select option:selected').attr('code');
      var wilayaPos = wilayaLatLong[wilayaCode];
      latitude = wilayaPos.lat;
      longitude = wilayaPos.lon;
      break;
    case 'manuel':
      var latitudeCoord = $("#latitude-textarea").val();
      var longitudeCoord = $("#longitude-textarea").val();
      var latitudeCoordMin = $("#lattd-min-textarea").val();
      var longitudeCoordMin = $("#longd-min-textarea").val();

      latitude = parseFloat(latitudeCoord) + parseFloat(latitudeCoordMin)/60;
      longitude = parseFloat(longitudeCoord) + parseFloat(longitudeCoordMin)/60;
      break;
    case 'map':
      var latitudeCoord = $("#latitude-map-input").val();
      var longitudeCoord = $("#longitude-map-input").val();
      var latitudeCoordMin = $("#lattd-min-map-input").val();
      var longitudeCoordMin = $("#longd-min-map-input").val();

      latitude = parseFloat(latitudeCoord) + parseFloat(latitudeCoordMin)/60;
      longitude = parseFloat(longitudeCoord) + parseFloat(longitudeCoordMin)/60;
      break;
  }
  resultat = {
    'lat' : latitude,
    'lon' : longitude
  }

  return resultat;
}

function calculMensuel(){


    var date = new Date( $('#date').val() );

    // récupérer la Date
    var mois = date.getMonth() +1 ; //0 est janvier
    var jour = date.getDate();

    //récupérer les coordonnées
    var longitude, latitude;
    var resultat = getLatLonFromDisplay();
    longitude = resultat.lon;
    latitude = resultat.lat;


    //Données en sortie
    var tableAffichage  = $('#table-resultat-mois');
    $('#calcul-mensuel-affichage').slideUp('slow', function(){
      tableAffichage.text("");
      //On fait les calculs pour chaque jour du mois
      for(S = parseInt(tableDeConversionMoisJour[mois]) + 1; S <= parseInt(tableDeConversionMoisJour[mois+1]); S++){
        var dateDisplay = (S - parseInt(tableDeConversionMoisJour[mois])) + ' - ' + affichageMois[mois];
        //calcul
        var resultat = calculPmLevCou(S, latitude, longitude);
        var PM = resultat.PM;
        var LEV = resultat.LEV;
        var COU = resultat.COU;
        //Display
        PMh   = parseInt(PM) ;
        COUh  = parseInt(COU) ;
        LEVh  = parseInt(LEV) ;
        PMm   = parseInt((PM % 1) * 60) ;
        COUm  = parseInt((COU % 1) * 60) ;
        LEVm  = parseInt((LEV % 1) * 60) ;
        PMs   = parseInt(((PM % 1) * 60) % 1 * 60) ;
        COUs  = parseInt(((COU % 1) * 60) % 1 * 60) ;
        LEVs  = parseInt(((LEV % 1) * 60) % 1 * 60) ;

        if(isNaN(PM)||isNaN(LEV)||isNaN(COU)){
          PMh = '--';
          COUh = '--';
          LEVh = '--';
          PMm = '--';
          COUm = '--';
          LEVm = '--';
          PMs = '--';
          COUs = '--';
          LEVs = '--';
          tableAffichage.addClass('redColor');
        }else{
          tableAffichage.removeClass('redColor');
        }
        //Ligne de tableau à ajouter
        var tableLineHTML =
        "<tr>"+
          "<th scope='row'>" + dateDisplay + "</th>"+
          "<td>"+ LEVh +' h ' + LEVm + ' mn ' + LEVs +' s' +"</td>"+
          "<td>"+ PMh +' h ' + PMm + ' mn ' + PMs +' s' +"</td>"+
          "<td>"+ COUh +' h ' + COUm + ' mn ' + COUs +' s' +"</td>"+
        "</tr>";
        //Mise à jour des données en sortie
        tableAffichage.append(tableLineHTML);
      }//boucle de tous les jours du mois
      $('#calcul-mensuel-affichage').slideDown('slow');

    });

}
function calculQuotidien(){
  var date = new Date( $('#date').val() );

  // récupérer la Date
  var mois = date.getMonth() +1 ; //0 est janvier
  var jour = date.getDate();

  //récupérer les coordonnées
  var longitude, latitude;
  var resultat = getLatLonFromDisplay();
  longitude = resultat.lon;
  latitude = resultat.lat;

  //Données en sortie
  var PMLabel  = $('#meridien-soleil-label');
  var COULabel = $('#couche-soleil-label');
  var LEVLabel = $('#leve-soleil-label');


  //R2
  var S         = parseInt(tableDeConversionMoisJour[mois]) + parseInt(jour);
  //calcul
  var resultat = calculPmLevCou(S, latitude, longitude);
  var PM = resultat.PM;
  var LEV = resultat.LEV;
  var COU = resultat.COU;
  //Display
  PMh   = parseInt(PM) ;
  COUh  = parseInt(COU) ;
  LEVh  = parseInt(LEV) ;
  PMm   = parseInt((PM % 1) * 60) ;
  COUm  = parseInt((COU % 1) * 60) ;
  LEVm  = parseInt((LEV % 1) * 60) ;
  PMs   = parseInt(((PM % 1) * 60) % 1 * 60) ;
  COUs  = parseInt(((COU % 1) * 60) % 1 * 60) ;
  LEVs  = parseInt(((LEV % 1) * 60) % 1 * 60) ;

  if(isNaN(PM)||isNaN(LEV)||isNaN(COU)){
    PMh = '--';
    COUh = '--';
    LEVh = '--';
    PMm = '--';
    COUm = '--';
    LEVm = '--';
    PMs = '--';
    COUs = '--';
    LEVs = '--';
  }



  //Mise à jour des données en sortie
  $('#calcul-quotidien-affichage').slideUp(function(){
    LEVLabel.text(LEVh+' h ' + LEVm + ' mn ' + LEVs +' s');
    PMLabel.text(PMh+' h ' + PMm + ' mn ' + PMs +' s');
    COULabel.text(COUh+' h ' + COUm + ' mn ' + COUs +' s');
    $(this).slideDown();
  });

  if (PMh == '--'){
    PMLabel.addClass('redColor');
    COULabel.addClass('redColor');
    LEVLabel.addClass('redColor');
  }else{
    PMLabel.removeClass('redColor');
    COULabel.removeClass('redColor');
    LEVLabel.removeClass('redColor');
  }



}
