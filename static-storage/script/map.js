
var markerIconUrl = $('#map-div').attr('marker-icon-url');

var iconStyle = new ol.style.Style({
image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
  anchor: [0.5, 32],
  anchorXUnits: 'fraction',
  anchorYUnits: 'pixels',
  opacity: 0.75,
  src: markerIconUrl
}))
});


var map = new ol.Map({
    target: 'map-div',
    renderer: 'canvas',

    layers: [
        new ol.layer.Tile({source: new ol.source.OSM()})
    ],
    view: new ol.View({
        //projection: 'EPSG:900913',
        center: ol.proj.transform([3, 35.5], 'EPSG:4326', 'EPSG:3857'),
        zoom: 7
    })

});
//Controls
var fullScreenControl = new ol.control.FullScreen();
var overviewMapControl = new ol.control.OverviewMap();
var rotateControl = new ol.control.Rotate();
// var scaleLineControl = new ol.control.ScaleLine();
// var zoomSliderControl = new ol.control.ZoomSlider();
map.addControl(fullScreenControl);
map.addControl(overviewMapControl);
map.addControl(rotateControl);
// map.addControl(scaleLineControl);
// map.addControl(zoomSliderControl);



map.on('click', function(evt) {
  var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
  var lon = lonlat[0];
  var lat = lonlat[1];

  var iconFeature = new ol.Feature({
  geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857')),
  });
  iconFeature.setStyle(iconStyle);

  var vectorSource = new ol.source.Vector({
  features: [iconFeature]
  });

  var vectorLayer = new ol.layer.Vector({
  name : 'marker',
  source: vectorSource
  });

  map.getLayers().forEach(function (layer) {
    if (layer.values_.name == 'marker') {
        map.removeLayer(layer);
    }
  });
  map.addLayer(vectorLayer);

  map.updateSize();

  var lonDeg = parseInt(lon);
  var lonMin = parseInt((lon % 1) *60);
  var latDeg = parseInt(lat);
  var latMin = parseInt((lat % 1) *60);

  mapPositionHTML = $('#map-latlon-div');
  mapPositionLabels =
  "<br>"+
  "<br>"+
  "<br>"+
  "<div class='row'>"+
    "<div class='col-sm-2 col-sm-offset-3'>"+
      "<label>Latitude</label>"+
    "</div>"+
    "<div class='col-sm-5 '>"+
      '<input type="number" step="1" max="180" min="-180" id="longitude-map-input" class="non-resizeable predifined-size"  placeholder="Longitude" value='+ lonDeg +'></input>' +
        " <label> ° </label> "+
      '<input type="number" step="1" max="59" min="0" id="longd-min-map-input" class="non-resizeable predifined-size" placeholder="Longitude min" value='+ lonMin +'></input>'+
        " <label> ' </label> "+
      "</div>"+
  "</div>"+

  "<div class='row'>"+
    "<div class='col-sm-2 col-sm-offset-3'>"+
      "<label>Longitude</label>"+
    "</div>"+
      "<div class='col-sm-5 '>"+
        "<input type='number' step='1' max='90' min='-90' id='latitude-map-input' class='non-resizeable predifined-size' placeholder='Lattitude' value="+ latDeg +"></input>"+
          " <label> ° </label> "+
        "<input type='number' step='1' max='59' min='0' id='lattd-min-map-input' class='non-resizeable predifined-size' placeholder='Lattitude min' value="+ latMin +"></input>"+
          " <label> ' </label> "+
      "</div>"+
    "</div>"+
    "<br>";
  mapPositionHTML.fadeOut(function(){
    mapPositionHTML.text("");
    mapPositionHTML.append(mapPositionLabels);
    mapPositionHTML.fadeIn();

  });

  $('#map-div').addClass('position-selected');

});
