//tableau de hachage
var tableDeConversionMoisJour = [0,0,31,59,90,120,151,181,212,243,273,304,334,365];
var affichageMois = ['---','janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre'];
var wilayaLatLong = {
  '1':{
    'lat':27.89972,
    'lon':-0.28306,
  },
  '2':{
    'lat':36.16472,
    'lon':1.335,
  },
  '3':{
    'lat':33.79972,
    'lon':2.88306,
  },
  '4':{
    'lat':35.8775,
    'lon':7.11361,
  },
  '5':{
    'lat':35.55528,
    'lon':6.17861,
  },
  '6':{
    'lat':36.75,
    'lon':5.08306,
  },
  '7':{
    'lat':34.85,
    'lon':5.73306,
  },
  '8':{
    'lat':31.61667,
    'lon':-2.21667,
  },
  '9':{
    'lat':36.46667,
    'lon':2.83306,
  },
  '10':{
    'lat':36.38,
    'lon':3.90139,
  },
  '11':{
    'lat':22.78306,
    'lon':5.51667,
  },
  '12':{
    'lat':35.40417,
    'lon':8.12417,
  },
  '13':{
    'lat':34.87833,
    'lon':-1.315,
  },
  '14':{
    'lat':35.37583,
    'lon':1.31306,
  },
  '15':{
    'lat':36.71694,
    'lon':4.04972,
  },
  '16':{
    'lat':36.76306,
    'lon':3.05056,
  },
  '17':{
    'lat':34.66667,
    'lon':3.25,
  },
  '18':{
    'lat':36.79972,
    'lon':5.76667,
  },
  '19':{
    'lat':36.19139,
    'lon':5.40944,
  },
  '20':{
    'lat':34.83306,
    'lon':0.15,
  },
  '21':{
    'lat':36.86222,
    'lon':6.94722,
  },
  '22':{
    'lat':35.19389,
    'lon':-0.64417,
  },
  '23':{
    'lat':36.89972,
    'lon':7.76667,
  },
  '24':{
    'lat':36.46611,
    'lon':7.43389,
  },
  '25':{
    'lat':36.365,
    'lon':6.61472,
  },
  '26':{
    'lat':36.26667,
    'lon':2.75,
  },
  '27':{
    'lat':35.93306,
    'lon':0.09028,
  },
  '28':{
    'lat':35.72556,
    'lon':4.52778,
  },
  '29':{
    'lat':35.39444,
    'lon':0.13972,
  },
  '30':{
    'lat':31.94972,
    'lon':5.33306,
  },
  '31':{
    'lat':35.69111,
    'lon':-0.64167,
  },
  '32':{
    'lat':33.68611,
    'lon':1.01389,
  },
  '33':{
    'lat':26.48306,
    'lon':8.46667,
  },
  '34':{
    'lat':36.06667,
    'lon':4.76667,
  },
  '35':{
    'lat':36.76667,
    'lon':3.47722,
  },
  '36':{
    'lat':36.76861,
    'lon':8.31667,
  },
  '37':{
    'lat':27.67417,
    'lon':-8.31667,
  },
  '38':{
    'lat':35.60778,
    'lon':1.81111,
  },
  '39':{
    'lat':33.33306,
    'lon':6.88306,
  },
  '40':{
    'lat':35.43583,
    'lon':7.14333,
  },
  '41':{
    'lat':36.26861,
    'lon':7.93556,
  },
  '42':{
    'lat':36.58306,
    'lon':2.45222,
  },
  '43':{
    'lat':36.45111,
    'lon':6.26528,
  },
  '44':{
    'lat':36.06667,
    'lon':4.54972,
  },
  '45':{
    'lat':33.26667,
    'lon':-0.31667,
  },
  '46':{
    'lat':35.3125,
    'lon':-1.14528,
  },
  '47':{
    'lat':32.48306,
    'lon':3.66667,
  },
  '48':{
    'lat':35.7425,
    'lon':0.55917,
  }
}
